var listenners = {};
var ContantData = {};
var activeLang = "en";
var verifyCaptcha = false;
var urls = [
  "http://greatratesforyou.azurewebsites.net/api/apigr/signupmodel",
  "http://greatratesforyou.azurewebsites.net/api/apigr/fullleadform",
  "http://greatratesforyou.azurewebsites.net/api/apigr/PostSelectedRates",
  "http://greatratesforyou.azurewebsites.net/api/apigr/createAgent"
];
var app = new Vue({
  el: "#app",
  data: {
    Counties: [],
    HomeValues: [],
    Years: [],
    ConstructionType: [],
    DiscountItems: [],
    ZipCodes: [],
    LeadId: null,
    Rates: [],
    search: '',
    counter: 0
  },
  watch: {
    'Rates': function(val, oldVal) {
      this.$nextTick(() => {
        lang();
      })
    }
  },
  methods: {
    searchF() {
      this.search = this.$refs.searchvalue.value
    },
    selectedZipcode(zipCode) {
      this.$refs.searchvalue.value = zipCode;
      this.search = '';
    },
    searchChange(event) {
      if ($(event.target).hasClass('SearchInput') === false && $(event.target).hasClass('zipCodeResultItem') === false) {
        this.search = '';
      }
    },
  },
  computed: {
    ZipCodesSearch() {
      return this.ZipCodes.filter(post => {

        if (this.search.toString().length < 1) {
          return false
        } else {
          console.log(post.Id.toString().toLowerCase());
          if(post.Id.toString().toLowerCase().indexOf(this.search.toString().toLowerCase()) >= 0) {
              return true;
          }else{
            return false;
          }
           // post.Id.toString().toLowerCase().includes()
        }

      })
    }
  }
});
listenners.start = () => {
  modal();
  validateForm();
  lang();
  changeLang();
  loadData();
  $('.phone_us').mask('(000) 000-0000');
generalFunctions();
}

listenners.responsive = () => {
  $(window).resize(function() {

  });
}

function loadData() {
  $.ajax({
      method: "GET",
      url: urls[0] + "?lang=" + activeLang
    })
    .done(function(data) {
      data = data;
      app.Counties = data.Counties
      app.HomeValues = data.HomeValues
      app.Years = data.Years
      app.ConstructionType = data.ConstructionType
      app.DiscountItems = data.DiscountItems
      app.ZipCodes = data.ZipCodes
    })
    .fail(function(a, b, c) {
      console.log("error");
    });
}

function sendData(data, url, NextWindow, callBack) {
  data = JSON.stringify(data);
  $.ajax({
      method: "POST",
      url: url,
      data: data,
      contentType: 'application/json',
    })
    .done(function(data) {
      ContantData = {};
      if (typeof callBack !== null || typeof callBack !== undefined) {
        callBack(data);
      }
      activeWindow(NextWindow);

    })
    .fail(function(a, b, c) {;
    });
}

function modal() {
  $(".show-modal").on("click", function() {
    var modalId = $(this).data("target")
    var modalWidth = $(this).data("width");
    var modalHeight = $(this).data("height");
    $(modalId).find(".modal-contant").css({
      "max-width": modalWidth,
      "max-height": modalHeight,
    });
    $(modalId).addClass("show");
  });
  $(".modal-close, .modal").on("click", function(e) {
    if ($(e.target).hasClass("modal") || $(e.target).hasClass("modal-close")) {
      $(".modal").removeClass("show");
    }
  });
}

function validateForm() {
  $(".sendForm").on("click", function() {
    var ContainerForm = $(this).data("target")
    var NextWindow = $(this).data("nextwindow");
    var action = $(this).data("action");
    var classes = validateClass();
    var counterError = 0;
    $(ContainerForm + " input , " + ContainerForm + " select , " + ContainerForm + " textarea ").each(function() {
      for (var i = 0; i < classes.length; i++) {
        if ($(this).hasClass(classes[i])) {
          if (i === 0) {
            if (!isEmail($(this).val())) {
              counterError++
              $(this).parent().addClass("error");
            } else {
              $(this).parent().removeClass("error");
            }
          } else if (i === 1) {
            if (!noEmpty($(this).val())) {
              counterError++;
              $(this).parent().addClass("error");
            } else {
              $(this).parent().removeClass("error");
            }
          } else if (i === 2) {
            if (!isZip($(this).val())) {
              counterError++
              $(this).parent().addClass("error");
            } else {
              $(this).parent().removeClass("error");
            }
          }

        }
      }
    });
    if ($(".terms").length > 0) {
      if ($(".terms").prop("checked") === false) {
        counterError++;
        $(".terms").parent().parent().addClass("error");
      } else {
        $(".terms").parent().parent().removeClass("error");
      }
    }

    if ($(ContainerForm + " .g-recaptcha").length > 0) {
      if(verifyCaptcha === false){
        counterError++;
        $(ContainerForm + " .g-recaptcha").parent().addClass("error");
      }else{
        $(ContainerForm + ".g-recaptcha").parent().removeClass("error");
      }
    }



    if (counterError === 0) {
      var check = [];
      var checkName = "";
      var ratesCheck = {
        LeadId: null,
        Rates: []
      };
      $(ContainerForm + " input:not([type=checkbox]) , " + ContainerForm + " select , " + ContainerForm + " textarea ").each(function() {
        ContantData[$(this).attr('name')] = $(this).val();
      });
      if (action === 2) {
        $(ContainerForm + " input[type=checkbox]").each(function() {
          if ($(this).prop("checked")) {
            ratesCheck.Rates.push({
              "Company": $(this).data("company"),
              "Price": $(this).data("price")
            });
          }
        });
      } else {
        $(ContainerForm + " input[type=checkbox]").each(function() {
          if ($(this).prop("checked")) {
            check.push(parseInt($(this).val()))
          }
          checkName = $(this).attr('name');
        });
      }

      ContantData[checkName] = check;

      if (action === false || action === "false") {
        activeWindow("#loading");
        setTimeout(function() {
          activeWindow(NextWindow);
        }, 1000);
      } else {
        action = parseInt(action);
        activeWindow("#loading");
        if (action === 1) {
          sendData(ContantData, urls[action], NextWindow, function(data) {
            app.LeadId = data.LeadId;
            app.Rates = data.Rates;
          });
        } else if (action === 2) {
          ratesCheck.LeadId = app.LeadId;
          sendData(ratesCheck, urls[action], NextWindow, function(data) {});
        } else {
          sendData(ContantData, urls[action], NextWindow, function(data) {});
        }
      }

    }
  });
}

function activeWindow(id) {
  $(".steps-form-item").removeClass("active");
  $(id).addClass("active");

}

function changeLang() {
  $(".change-lang").on("click", function(e) {
    e.preventDefault();
    $(".lang-html").attr("lang", $(this).data("lang"));
    if (lang === undefined || lang === null) {
      lang = "en";
      activeLang = "en"
    }
    activeLang = lang;
    lang();
  });
}

function lang() {
  $(".change-lang").removeClass("active");
  var lang = $(".lang-html").attr("lang");

  loadData();
  $(".change-lang").each(function() {
    if ($(this).data("lang") === lang) {
      $(this).addClass("active");
    }
  });
  $(".change-lang-item").each(function() {
    if ($(this).data(lang) === undefined || $(this).data(lang) === null) {
      lang = "en";
    }
    if (this.tagName == "INPUT" || this.tagName == "TEXTAREA") {
      $(this).attr("placeholder", $(this).data(lang));
    } else {
      $(this).html($(this).data(lang));
    }
  });
}

var verifyCallback = function(response) {
  verifyCaptcha = true;
  $(" .g-recaptcha").parent().removeClass("error");
};

function generalFunctions() {
  $(".menu-main__toggle").on("click",function (e) {
    $(this).parent().toggleClass("open");
  });
  if ($(".body").length !== 0) {
      $(window).scroll(function() {
          if ($(this).scrollTop() >  1) {
              $(".main-header").addClass("in");
          }else{
              $(".main-header").removeClass("in");
          }
      });
  }
}
