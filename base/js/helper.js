function counting(element) {
  element.prop('Counter', 0).animate({
    Counter: element.data("number")
  }, {
    duration: 600,
    step: function(now) {
      $(this).find("a").text(Math.ceil(now).toLocaleString());
    }
  });
}

function responsiveOffset() {
  if (window.matchMedia("(max-width: 767px)").matches) {
    return 37;
  } else {
    return 0;
  }
}

$(document).on("click", ".clickTo", function(e) {

  e.preventDefault();
  // $($(this).data("target")).click();
  // console.log($());
  window.open($(this).data("url"), "_blank")
});



function validateClass() {
  return ["email","no-empty","zip"];
}

function  noEmpty(value) {
  if (value === null || value === undefined || value === "" || value === " ") {
    return false;
  }
  return true;
}
function isEmail(email) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
}
function isZip(zip) {
    var re =/(^\d{5}$)|(^\d{5}-\d{4}$)/;
    return re.test(String(zip));
}
